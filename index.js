const express = require("express");
const mongoose = require("mongoose");

// server preperations
const app = express();
const port = 5001;

// [SECTION] - MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.fhytgag.mongodb.net/b229_todo?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

// set notification for success || failure

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("Connected to cloud database.")); /*similar to listen function*/

/*

	[SECTION] - Schemas
	Schemas determine the structure of the documents to be written in the database
		Schemas act as blueprints to our data
		Use the Schema() -case sensitive - must be capital- constructor of the Mongoose module to create a new Schema object
		The "new" -similar to class constructor- keyword creates a new Schema

*/

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name is required~!"]
	},
	status: {
		type: String,
		default: "pending"	
	}
});

	const userSchema = new mongoose.Schema({
		username: {
			type: String,
			required: [true, "Username is required!"]
		},
		password: {
			type: String,
			required: [true, "Password is required!"]
		}
	});

// [SECTIONS] - Models (case sensitive - must be capital)


const User = mongoose.model("User", userSchema);

const Task = mongoose.model("Task", taskSchema);

// middlewares
app.use(express.json()); /*converts data express to json*/
app.use(express.urlencoded({extended: true}));

// Creating a New Task

// Business Logic
		/*
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
		*/


app.post("/tasks", (req, res) => {
	// Task knows the data/operations of Schema
	// Schema knows the properties in the database (.name)
	Task.findOne({name: req.body.name}, (err,result) => {
		if(result != null && result.name == req.body.name) {
			return res.send("Duplicate task found");	
		} else {
			let newTask = new Task({
				// foloows blooprints from taskSchema
				name: req.body.name
				// holds the data and saves below (newTask)
			});
			// saveErr is a backup when error occurs
			newTask.save((saveErr, saveTask) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New Task created!");
				}
			})
		}
	})
});


// Getting all tasks

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
});





app.post("/user-signup", (req, res) => {
	User.findOne({username: req.body.username}, (err,result) => {
		if(result != null && result.username == req.body.username) {
			return res.send("User is taken!");	
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr, saveUser) => {
				if(saveErr) {
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user signed!");
				}
			})
		}
	})
});


app.get("/user-signup", (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				users: result
			})
		}
	})
});







app.listen(port, () => console.log(`Server running at ${port}.`));


